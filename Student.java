public class Student {
	//fields
	public String id;
	public String program;
	public double rScore;
	
	//uses id to display rScore
	public void displayScore() {
		System.out.println("Your R-Score is " + this.rScore);
	}
	
	//use id to display program
	public void printProgramLength() {
		System.out.print("The duration of this program is ");
		if (this.program.equals("Mechanical Engineering")) {
			System.out.println("6 semestres");
		} else {
			System.out.println("4 semestres");
		}
	}
}