public class Application {
	public static void main(String[] args) {
		Student jame = new Student();
		jame.id = "2381927";
		jame.program = "Pure & Applied Science";
		jame.rScore = 29.8;
		
		Student may = new Student();
		may.id = "1827364";
		may.program = "Mechanical Engineering";
		may.rScore = 36.4;
		
		Student[] section4 = new Student[3];
		section4[0] = jame;
		section4[1] = may;
		
		section4[2] = new Student();
		section4[2].id = "1234567";
		section4[2].program = "Studio Art";
		section4[2].rScore = 26.8;
		
		//System.out.println(section4[0].id);
		System.out.println(section4[2].id);
		System.out.println(section4[2].program);
		System.out.println(section4[2].rScore);
		
		/* jame.displayScore();
		jame.printProgramLength();
		
		System.out.println("");
		
		may.displayScore();
		may.printProgramLength(); */
	}
}